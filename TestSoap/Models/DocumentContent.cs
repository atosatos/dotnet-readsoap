﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSoap.Models
{
    public class DocumentContent
    {
        private long Id;
        private int? DocumentId;
        private int? ContentId;
        private int? SerialNo;
        private int Active;
        private int CreatorId;
        private DateTime CreationDate;
        private int? ModifierId;
        private DateTime ModificationDate;
        private Content Content;

        public long id { get => Id; set => Id = value; }
        public int? documentId { get => DocumentId; set => DocumentId = value; }
        public int? contentId { get => ContentId; set => ContentId = value; }
        public int? serialNo { get => SerialNo; set => SerialNo = value; }
        public int active { get => Active; set => Active = value; }
        public int creatorId { get => CreatorId; set => CreatorId = value; }
        public DateTime creationDate { get => CreationDate; set => CreationDate = value; }
        public int? modifierId { get => ModifierId; set => ModifierId = value; }
        public DateTime modificationDate { get => ModificationDate; set => ModificationDate = value; }
        public Content content { get => Content; set => Content = value; }
    }
}