﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSoap.Models
{
    public class WccFile
    {
        private string DocTitle;
        private string DocFile;
        private string DocName;

        public string docTitle { get => DocTitle; set => DocTitle = value; }
        public string docFile { get => DocFile; set => DocFile = value; }
        public string docName { get => DocName; set => DocName = value; }
    }
}