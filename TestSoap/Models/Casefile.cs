﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSoap.Models
{
    public class Casefile
    {
       private long Id;
    private int ClassId;
    private int CaseYear;
    private int CaseSn;
    private String CaseNum;
    private int OrgUnitId;
    private DateTime InitialDate;
    private int JudgeId;
    private int UnigueCaseSn;
    private String Unigue_case_num;
    private int Active;
    private int CreatorId;
    private DateTime CreationDate;
    private int StadiumCode;

        public long id { get => Id; set => Id = value; }
        public int classId { get => ClassId; set => ClassId = value; }
        public int caseYear { get => CaseYear; set => CaseYear = value; }
        public int caseSn { get => CaseSn; set => CaseSn = value; }
        public string caseNum { get => CaseNum; set => CaseNum = value; }
        public int orgUnitId { get => OrgUnitId; set => OrgUnitId = value; }
        public DateTime initialDate { get => InitialDate; set => InitialDate = value; }
        public int judgeId { get => JudgeId; set => JudgeId = value; }
        public int unigueCaseSn { get => UnigueCaseSn; set => UnigueCaseSn = value; }
        public string unigue_case_num { get => Unigue_case_num; set => Unigue_case_num = value; }
        public int active { get => Active; set => Active = value; }
        public int creatorId { get => CreatorId; set => CreatorId = value; }
        public DateTime creationDate { get => CreationDate; set => CreationDate = value; }
        public int stadiumCode { get => StadiumCode; set => StadiumCode = value; }
    }
}