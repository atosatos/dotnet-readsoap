﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSoap.Models
{
    public class Task
    {
        private string user;
        private string taskNumber;
        private string title;
        private DateTime assignedDate;
        private string casefileId;

        public string CasefileId { get { return casefileId; } set { casefileId = value; } }
        public string User { get { return user; } set { user = value; } }

        public DateTime AssignedDate { get { return assignedDate; } set { assignedDate = value; } }
        public string Title
        {
            get { return title; }
            set { title = value; }
        }


        public string TaskNumber
        {
            get { return taskNumber; }
            set { taskNumber = value; }
        }
    }
}