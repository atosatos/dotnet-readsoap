﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSoap.Models
{
    public class Content
    {
        private long Id;
        private int Active;
        private int CreatorId;
        private DateTime CreationDate;
        private int? ModifierId;
        private DateTime ModificationDate;
        private int? Version;
        private String FileName;
        private String FileLocation;

        public long id { get => Id; set => Id = value; }
        public int active { get => Active; set => Active = value; }
        public int creatorId { get => CreatorId; set => CreatorId = value; }
        public DateTime creationDate { get => CreationDate; set => CreationDate = value; }
        public int? modifierId { get => ModifierId; set => ModifierId = value; }
        public DateTime modificationDate { get => ModificationDate; set => ModificationDate = value; }
        public int? version { get => Version; set => Version = value; }
        public string fileName { get => FileName; set => FileName = value; }
        public string fileLocation { get => FileLocation; set => FileLocation = value; }
    }
}