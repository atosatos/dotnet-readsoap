﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestSoap.Models
{
    public class Document
    {
        private long Id;
        private String SenderTypeId;
        private int? DeliveryTypeId;
        private String SenderName;
        private DateTime DocumentDate;
        private DateTime InitialActDate;
        private String  Name;
        private String Description;
        private int? ExternalClassId;
        private int? Year;
        private DateTime ExtetnalDocumentDate;
        private String ExternalNumber;
        private int DocumentTypeId;
        private int? DocumentSubtypeId;
        private String DocumentNumber;
        private int? PersonId;
        private int? DocumentOutComeId;
        private int CasefileId;
        private int Active;
        private int CreatorId;
        private DateTime CreationDate;
        private int? ModifierId;
        private DateTime ModificationDate;
        private List<DocumentContent> DocumentContents;


        public long id { get => Id; set => Id = value; }
        public string senderTypeId { get => SenderTypeId; set => SenderTypeId = value; }
        public int? deliveryTypeId { get => DeliveryTypeId; set => DeliveryTypeId = value; }
        public string senderName { get => SenderName; set => SenderName = value; }
        public DateTime documentDate { get => DocumentDate; set => DocumentDate = value; }
        public DateTime initialActDate { get => InitialActDate; set => InitialActDate = value; }
        public string name { get => Name; set => Name = value; }
        public string description { get => Description; set => Description = value; }
        public int? externalClassId { get => ExternalClassId; set => ExternalClassId = value; }
        public int? year { get => Year; set => Year = value; }
        public DateTime extetnalDocumentDate { get => ExtetnalDocumentDate; set => ExtetnalDocumentDate = value; }
        public string externalNumber { get => ExternalNumber; set => ExternalNumber = value; }
        public int documentTypeId { get => DocumentTypeId; set => DocumentTypeId = value; }
        public int? documentSubtypeId { get => DocumentSubtypeId; set => DocumentSubtypeId = value; }
        public string documentNumber { get => DocumentNumber; set => DocumentNumber = value; }
        public int? personId { get => PersonId; set => PersonId = value; }
        public int? documentOutComeId { get => DocumentOutComeId; set => DocumentOutComeId = value; }
        public int casefileId { get => CasefileId; set => CasefileId = value; }
        public int active { get => Active; set => Active = value; }
        public int creatorId { get => CreatorId; set => CreatorId = value; }
        public DateTime creationDate { get => CreationDate; set => CreationDate = value; }
        public int? modifierId { get => ModifierId; set => ModifierId = value; }
        public DateTime modificationDate{ get => ModificationDate; set => ModificationDate = value; }
        public List<DocumentContent> documentContents { get => DocumentContents; set => DocumentContents = value; }

    }
}