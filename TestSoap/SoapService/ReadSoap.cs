﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using TestSoap.TaskQueryService;


namespace TestSoap.SoapService
{
    public class ReadSoap
    {
        private static string usernameUser = null;
        private static string usernameServer = "weblogic";
        private static string passwordServer = "welcome321";
        private static workflowContextType ctx;
        private static TaskQueryService.TaskQueryServiceClient tqs;

        public void loginUser(string username) {
            init();
            tqs = new TaskQueryServiceClient("TaskQueryServicePort");
            // provide credentials for ws-security authentication to WLS to call the web service
            tqs.ClientCredentials.UserName.UserName = usernameServer;
            tqs.ClientCredentials.UserName.Password = passwordServer;

            // set up the application level credentials that will be used to get a session on BPM (not WLS)
            credentialType cred = new credentialType();
            cred.login = username;
            cred.password = username;
            cred.identityContext = "jazn.com";
            usernameUser = username;

            // authenticate to BPM
            Console.WriteLine("Authenticating...");
            ctx = tqs.authenticate(cred);
        }
        public task[] getAllTask() {

            // now we need to build the request ... there is a whole bunch of stuff
            // we have to specify in here ... a WHOLE bunch of stuff...
            taskListRequestType request = new taskListRequestType();
            request.workflowContext = ctx;
            // predicate
            taskPredicateQueryType pred = new taskPredicateQueryType();
            // predicate->order - e.g. ascending by column called "TITLE"
            orderingClauseType order = new orderingClauseType();
            order.sortOrder = sortOrderEnum.DESCENDING;
            order.nullFirst = false;
            order.Items = new string[] { "TITLE" };
            order.ItemsElementName = new ItemsChoiceType1[] { ItemsChoiceType1.column };
            orderingClauseType[] orders = new orderingClauseType[] { order };
            pred.ordering = orders;
            // predicate->paging controls - remember TQS.queryTasks only returns 200 maximum rows
            // you have to loop/page to get more than 20
            pred.startRow = "0";
            pred.endRow = "20";
            // predicate->task predicate
            taskPredicateType tpred = new taskPredicateType();
            // predicate->task predicate->assignment filter - e.g. "ALL" users
            tpred.assignmentFilter = assignmentFilterEnum.MyGroup;
            tpred.assignmentFilterSpecified = true;

            // predicate->task predicate->clause - e.g. column "STATE" equals "ASSIGNED"
            predicateClauseType[] clauses = new predicateClauseType[1];
            clauses[0] = new predicateClauseType();
            clauses[0].column = "STATE";
            clauses[0].@operator = predicateOperationEnum.EQ;
            clauses[0].Item = "ASSIGNED";
            tpred.Items = clauses;
            pred.predicate = tpred;
            // items->display columns
            displayColumnType columns = new displayColumnType();
            columns.displayColumn = new string[] { "TITLE" };
            // items->presentation id
            string presentationId = "";
            // items->optional info
            taskOptionalInfoType opt = new taskOptionalInfoType();
            object[] items = new object[] { columns, opt, presentationId };
            pred.Items = items;
            request.taskPredicateQuery = pred;

            // get the list of tasks
            task[] tasks = tqs.queryTasks(request);

            return tasks;
        }
        public Dictionary<string, string> getTask(string taskNumber,string titleTask) {
            string casefileId = "";
            string documentId = "";
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            // and the transport clientCredentialType to "Basic"
            tqs = new TaskQueryServiceClient("TaskQueryServicePort");
            // provide credentials for ws-security authentication to WLS to call the web service
            tqs.ClientCredentials.UserName.UserName = "weblogic";
            tqs.ClientCredentials.UserName.Password = "welcome321";

            // set up the application level credentials that will be used to get a session on BPM (not WLS)
            credentialType cred = new credentialType();
            cred.login = "ospg_sudija2";
            cred.password = "ospg_sudija2";
            cred.identityContext = "jazn.com";

            // authenticate to BPM
           if(ctx==null)
                 ctx = tqs.authenticate(cred);

            // now we need to build the request ... there is a whole bunch of stuff
            // we have to specify in here ... a WHOLE bunch of stuff...
            taskListRequestType request = new taskListRequestType();
            request.workflowContext = ctx;
            // predicate
            taskPredicateQueryType pred = new taskPredicateQueryType();
            // predicate->order - e.g. ascending by column called "TITLE"
            orderingClauseType order = new orderingClauseType();
            order.sortOrder = sortOrderEnum.DESCENDING;
            order.nullFirst = false;
            order.Items = new string[] { "TITLE" };
            order.ItemsElementName = new ItemsChoiceType1[] { ItemsChoiceType1.column };
            orderingClauseType[] orders = new orderingClauseType[] { order };
            pred.ordering = orders;
            // predicate->paging controls - remember TQS.queryTasks only returns 200 maximum rows
            // you have to loop/page to get more than 200
            pred.startRow = "0";
            pred.endRow = "20";
            // predicate->task predicate
            taskPredicateType tpred = new taskPredicateType();
            // predicate->task predicate->assignment filter - e.g. "ALL" users
            tpred.assignmentFilter = assignmentFilterEnum.MyGroup;
            tpred.assignmentFilterSpecified = true;
            // predicate->task predicate->clause - e.g. column "STATE" equals "ASSIGNED"
            predicateClauseType[] clauses = new predicateClauseType[1];
            clauses[0] = new predicateClauseType();
            clauses[0].column = "STATE";
            clauses[0].@operator = predicateOperationEnum.EQ;
            clauses[0].Item = "ASSIGNED";
            tpred.Items = clauses;
            pred.predicate = tpred;
            // items->display columns
            displayColumnType columns = new displayColumnType();
            columns.displayColumn = new string[] { "TITLE" };
            // items->presentation id
            string presentationId = "";
            // items->optional info
            taskOptionalInfoType opt = new taskOptionalInfoType();
            object[] items = new object[] { columns, opt, presentationId };
            pred.Items = items;
            request.taskPredicateQuery = pred;

            // get the list of tasks
            task[] tasks = tqs.queryTasks(request);
            taskDetailsByNumberRequestType requestT = new taskDetailsByNumberRequestType();
            requestT.taskNumber = taskNumber;
            requestT.workflowContext = ctx;
            task task = tqs.getTaskDetailsByNumber(requestT);
            System.Xml.XmlNode[] lista = (System.Xml.XmlNode[])task.payload;

            if (usernameUser.Contains("upisnicar"))
            {
                string innerXml = lista.FirstOrDefault().InnerText;
                string[] stringSeparators = new string[] { "\n" };
                string[] lines = innerXml.Split(stringSeparators, StringSplitOptions.None);

                if (titleTask != null) {
                    if (titleTask.Contains("Skeniranje_pismena"))
                    {
                        //check index of documentid and casefileId etc.
                        //add documentId
                        dictionary.Add("documentId", lines[14].Trim());
                        dictionary.Add("casefileId", lines[2].Trim());

                    }
                    else {
                        //check index of documentid and casefileId etc.
                        dictionary.Add("documentId", lines[1].Trim());
                        dictionary.Add("casefileId", lines[2].Trim());


                    }

                }


            }
            else if (usernameUser.Contains("sudija")) {
                //add casefileId
                dictionary.Add("casefileId", lista.FirstOrDefault().InnerText);

            }

            return dictionary;
        }

        public void approveTask(string number,string username) {
            //set up user credential 
            credentialType cred = new credentialType();
            cred.login = username;
            cred.password = username;
            cred.identityContext = "jazn.com";
            //connect to service 
            if (ctx==null)
                ctx = tqs.authenticate(cred);

            taskDetailsByNumberRequestType request = new taskDetailsByNumberRequestType();
            request.taskNumber = number;
            request.workflowContext = ctx;
            task task = tqs.getTaskDetailsByNumber(request);

            // get TaskService
            TestSoap.TaskService.TaskServiceClient ts = new TestSoap.TaskService.TaskServiceClient("TaskServicePort");

            // update the payload
            System.Xml.XmlNode[] payload = (System.Xml.XmlNode[])task.payload;
            //payload.ElementAt(0).ChildNodes.Item(1).InnerText = "changed in .net";
            //task.attachment
            task.payload = payload;
            Console.WriteLine(task.systemAttributes.taskNumber);
            // update task
            TestSoap.TaskService.taskServiceContextTaskBaseType updateTaskRequest = new TestSoap.TaskService.taskServiceContextTaskBaseType();
            updateTaskRequest.workflowContext = ctx;
            updateTaskRequest.task = task;
            
            
            
            TestSoap.TaskService.task updatedTask = ts.updateTask(updateTaskRequest);

            // complete task
            TestSoap.TaskService.updateTaskOutcomeType updateTaskOutcomeRequest = new TestSoap.TaskService.updateTaskOutcomeType();
            updateTaskOutcomeRequest.workflowContext = ctx;

            updateTaskOutcomeRequest.outcome = "APPROVE";
            updateTaskOutcomeRequest.Item = updatedTask;
            ts.updateTaskOutcome(updateTaskOutcomeRequest); ;
        }

        public void rejectTask(string number, string username)
        {
            //set up user credential 
            credentialType cred = new credentialType();
            cred.login = username;
            cred.password = username;
            cred.identityContext = "jazn.com";
            //connect to service 
            if (ctx == null)
                ctx = tqs.authenticate(cred);

            taskDetailsByNumberRequestType request = new taskDetailsByNumberRequestType();
            request.taskNumber = number;
            request.workflowContext = ctx;
            task task = tqs.getTaskDetailsByNumber(request);

            // get TaskService
            TestSoap.TaskService.TaskServiceClient ts = new TestSoap.TaskService.TaskServiceClient("TaskServicePort");

            // update the payload
            System.Xml.XmlNode[] payload = (System.Xml.XmlNode[])task.payload;
            //payload.ElementAt(0).ChildNodes.Item(1).InnerText = "changed in .net";
            //task.attachment
            task.payload = payload;
            Console.WriteLine(task.systemAttributes.taskNumber);
            // update task
            TestSoap.TaskService.taskServiceContextTaskBaseType updateTaskRequest = new TestSoap.TaskService.taskServiceContextTaskBaseType();
            updateTaskRequest.workflowContext = ctx;
            updateTaskRequest.task = task;

            TestSoap.TaskService.task updatedTask = ts.updateTask(updateTaskRequest);

            // complete task
            TestSoap.TaskService.updateTaskOutcomeType updateTaskOutcomeRequest = new TestSoap.TaskService.updateTaskOutcomeType();
            updateTaskOutcomeRequest.workflowContext = ctx;

            updateTaskOutcomeRequest.outcome = "REJECT";
            updateTaskOutcomeRequest.Item = updatedTask;
            ts.updateTaskOutcome(updateTaskOutcomeRequest); ;
        }

        private static void init()
        {
            // set up the automapper
            //set up for wsld from 78
            AutoMapper.Mapper.CreateMap<TaskQueryService.propertyType, TestSoap.TaskService.propertyType2>();

            AutoMapper.Mapper.CreateMap<TaskQueryService.propertyType, TestSoap.TaskService.propertyType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.commentType, TestSoap.TaskService.commentType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.identityType, TestSoap.TaskService.identityType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.attachmentType, TestSoap.TaskService.attachmentType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.ucmMetadataItemType, TestSoap.TaskService.ucmMetadataItemType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.documentType, TestSoap.TaskService.documentType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.processType, TestSoap.TaskService.processType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.systemAttributesType, TestSoap.TaskService.systemAttributesType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.actionType, TestSoap.TaskService.actionType2>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.displayInfoType, TestSoap.TaskService.displayInfoType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.shortHistoryTaskType, TestSoap.TaskService.shortHistoryTaskType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.assignmentContextType, TestSoap.TaskService.assignmentContextType1>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.assignmentContextTypeValueType, TestSoap.TaskService.assignmentContextTypeValueType1>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.collectionTargetType, TestSoap.TaskService.collectionTargetType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.collectionTargetActionType, TestSoap.TaskService.collectionTargetActionType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.preActionUserStepType, TestSoap.TaskService.preActionUserStepType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.hiddenAttributesType, TestSoap.TaskService.hiddenAttributesType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.systemMessageAttributesType, TestSoap.TaskService.systemMessageAttributesType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.systemAttributesType, TestSoap.TaskService.systemAttributesType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.flexfieldMappingType, TestSoap.TaskService.flexfieldMappingType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.scaType, TestSoap.TaskService.scaType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.UpdatableEvidenceAttributesType, TestSoap.TaskService.UpdatableEvidenceAttributesType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.callbackType, TestSoap.TaskService.callbackType1>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.EvidenceType, TestSoap.TaskService.EvidenceType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.customAttributesType, TestSoap.TaskService.customAttributesType>();

            ///AutoMapper.Mapper.CreateMap<TaskQueryService.propertyType, TaskService.propertyType2>();

            AutoMapper.Mapper.CreateMap<TaskQueryService.workflowContextType, TestSoap.TaskService.workflowContextType>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.task, TestSoap.TaskService.task>();
            AutoMapper.Mapper.CreateMap<TaskQueryService.credentialType, TestSoap.TaskService.credentialType>();


            // check automapper config is valid
            AutoMapper.Mapper.AssertConfigurationIsValid();
        }

    }
    namespace TaskService
    {
        partial class workflowContextType
        {
            public static implicit operator workflowContextType(TaskQueryService.workflowContextType from)
            {
                return AutoMapper.Mapper.Map<TaskQueryService.workflowContextType, workflowContextType>(from);
            }
        }

        partial class task
        {
            public static implicit operator task(TaskQueryService.task from)
            {
                return AutoMapper.Mapper.Map<TaskQueryService.task, task>(from);
            }
        }
    }
}