﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TestSoap.Models;

namespace TestSoap.SoapService
{
    public class RestApi
    {
        string Baseurl = "http://10.194.2.200:8080";

        public  Casefile GetCasefileAsync(string casefile)
        {
            Casefile lista = null;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(Baseurl);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));



            // List data response.
            HttpResponseMessage response = client.GetAsync("/MongoSpringRest-0.0.1-SNAPSHOT/getCasefile/" + casefile).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (response.IsSuccessStatusCode)
            {
                var CasefileResponse = response.Content.ReadAsStringAsync().Result;
                lista = JsonConvert.DeserializeObject<Casefile>(CasefileResponse);
            }

            // Make any other calls using HttpClient here.

            // Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
            client.Dispose();
            return lista;
        }

        public CasefileResponse PostCasefileAsync(string casefile)
        {
            CasefileResponse lista = null;
            CasefileResponse responsefile = null;

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(Baseurl);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            //request get casefile by id
            HttpResponseMessage response = client.GetAsync("/MongoSpringRest-0.0.1-SNAPSHOT/getCasefile/" + casefile).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (response.IsSuccessStatusCode)
            {
                var CasefileResponse = response.Content.ReadAsStringAsync().Result;
                lista = JsonConvert.DeserializeObject<CasefileResponse>(CasefileResponse);
            }
            //convert casefileresponse to casefile for post method
            Casefile postCase = new Casefile()
            {
                active = lista.active,
                caseNum = lista.caseNum,
                caseSn = lista.caseSn,
                caseYear = lista.caseYear,
                classId = lista.classId,
                creationDate = lista.creationDate,
                creatorId = lista.creatorId,
                id = lista.id,
                initialDate = lista.initialDate,
                judgeId = lista.judgeId,
                orgUnitId = lista.orgUnitId,
                unigueCaseSn = lista.unigueCaseSn,
                unigue_case_num = lista.unigue_case_num,
                //update stadium
                stadiumCode = 2
            };

            //convert object to json
            var myContent = JsonConvert.SerializeObject(postCase);
            var stringContent = new StringContent(myContent, UnicodeEncoding.UTF8, "application/json");

            //Post method
             HttpResponseMessage postResponse = client.PostAsync("/MongoSpringRest-0.0.1-SNAPSHOT/saveCasefile", stringContent).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
               if (postResponse.IsSuccessStatusCode)
               {
                //post response
                   var casefileResponse = postResponse.Content.ReadAsStringAsync().Result;
                //read post response
                    responsefile = JsonConvert.DeserializeObject<CasefileResponse>(casefileResponse);
               }
             
           

            // Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
            client.Dispose();
            return responsefile;
        }
    }
           
}