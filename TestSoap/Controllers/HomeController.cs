﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestSoap.Models;
using TestSoap.TaskQueryService;
using System.IO;
using TestSoap.WccService;
using System.Threading;

namespace TestSoap.Controllers
{
    public class HomeController : Controller
    {



        public ActionResult Index()        {

            return View();
        }
     
        public ActionResult About()
        {

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Task(string number,string title,string username)
        {
            SoapService.RestApi api = new SoapService.RestApi();
            ViewBag.Number = number;
            ViewBag.Predmet = title;
            SoapService.ReadSoap rs = new SoapService.ReadSoap();
            Dictionary<string , string> dictionary = rs.getTask(number,null);
            string casefile = dictionary["casefileId"];
            ViewBag.Casefile = casefile;
           
            Casefile cf = api.GetCasefileAsync(casefile);
            ViewBag.Lista = cf;
            ViewBag.User = username;

            return View();
        }

        public ActionResult Registar(string number, string title, string username)
        {
            string casefile = "";
            string documentId = "";
            SoapService.RestApi api = new SoapService.RestApi();
            ViewBag.Number = number;
            ViewBag.Predmet = title;
            ViewBag.TitleTask = title.Replace(" ","_");

            SoapService.ReadSoap rs = new SoapService.ReadSoap();
            Dictionary<string, string> dictionary = rs.getTask(number, @ViewBag.TitleTask);
            if(dictionary["casefileId"]!=null)
                casefile= dictionary["casefileId"];
            if (dictionary["documentId"] !=null)
                documentId = dictionary["documentId"];
            ViewBag.Casefile = casefile;
            ViewBag.Document = documentId;


            Casefile cf = api.GetCasefileAsync(casefile);
            ViewBag.Lista = cf;
            ViewBag.User = username;

            return View();
        }



        [HttpPost]
        public ActionResult Subscribe(Task model)
        {
            if (ModelState.IsValid)
            {
                //TODO: SubscribeUser(model.Email);
                SoapService.ReadSoap r = new SoapService.ReadSoap();
               String cf = "PROBA";
                r.loginUser(model.User);
                task[] list = r.getAllTask();
                List<Task> l = new List<Task>();
                foreach (task t in list)
                {
                    l.Add(new Task()
                    {
                        Title = t.title,
                        TaskNumber = t.systemAttributes.taskNumber,
                        AssignedDate = t.systemAttributes.assignedDate
                    });
                }
                ViewBag.Message = "Your application description page. " + cf;
                ViewBag.Task = l;
                ViewBag.User = model.User;
            }

            return View("About", model);
        }

        [HttpPost]
        public ActionResult Approve(string number,string user,string casefile)
        {
            if (ModelState.IsValid)
            {
                SoapService.RestApi api = new SoapService.RestApi();

                SoapService.ReadSoap r = new SoapService.ReadSoap();
               // r.approveTask(number,user);
               // api.PostCasefileAsync(casefile);
                // System.Diagnostics.Process.Start("chrome.exe","http://oraclemojcg2:7003/workflow/ZaduzenjPredmetaServisUI/faces/adf.task-flow?bpmWorklistTaskId=004e47bd-e4eb-491b-8bdd-a1ba590483e3&bpmWorklistContext=[user=ospg_sudija2][sessionKey=3cd128b2-0f39-4690-b58c-5573b55dbc34][tokenType=G][requester=ospg_sudija2][objectId=null]&bpmWorklistHttpURL=http%3A%2F%2Foraclemojcg2%3A7003%2FISPConsole%2Ffaces&bpmWorklistHome=http%3A%2F%2Foraclemojcg2%3A7003%2FISPConsole%2Ffaces&tz=Europe%2FBelgrade&lg=en&cy=US&vr=&dispNameLg=en&sf=alta&acMode=default&bpmWorklistSessionTimeoutInterval=900&bpmBrowserWindowStatus=taskFlowReturn&enableNavigationButton=true&adf.tfDoc=%2FWEB-INF%2FZaduzenjePredmetaHT_TaskFlow.xml&adf.tfId=ZaduzenjePredmetaHT_TaskFlow");

            }

            return View("Index");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file,string number,string title, string documentId)
        {
            if (file != null && file.ContentLength > 0)
            {

                SoapService.ReadSoap rs = new SoapService.ReadSoap();
              


                BinaryReader b = new BinaryReader(file.InputStream);
                byte[] binData = b.ReadBytes(file.ContentLength);
                string contentFile = System.Text.Encoding.UTF8.GetString(binData);
                string filename = Path.GetFileName(file.FileName);
                TestSoap.WccService.WccService wccservice = new TestSoap.WccService.WccService();
                string result = wccservice.postFileToWcc(documentId, contentFile);
                Thread.Sleep(5000);
                string locId = wccservice.getLocationId(documentId);


            }
            //HttpPostedFileBase file = Request.Files[0 ];

            return View("Index");

        }

    }
}