﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using TestSoap.Models;

namespace TestSoap.WccService
{
    public class WccService
    {
        string Baseurl = "http://10.194.2.88:16200";

        public string getLocationId(string documentId)
        {
            string lista = "Daj nesto";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(Baseurl);

            // Add an Accept header for JSON format.

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
 


           // casefile = "4836";
            // List data response.
            HttpResponseMessage response = client.GetAsync("/Wcc_Rest-0.0.1-SNAPSHOT/getData/" + documentId).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (response.IsSuccessStatusCode)
            {
                var CasefileResponse = response.Content.ReadAsStringAsync().Result;
                lista = JsonConvert.DeserializeObject<string>(CasefileResponse);

            }

            // Make any other calls using HttpClient here.

            // Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
            client.Dispose();
            return lista;
        }


        public string postFileToWcc(string documentId,string content)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(Baseurl);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
           // documentId = "8002";
            WccFile file = new WccFile()
            {
                docFile =content,
                docName =documentId,
                docTitle = documentId
            };



            //convert object to json
            var myContent = JsonConvert.SerializeObject(file);
            var stringContent = new StringContent(myContent, UnicodeEncoding.UTF8, "application/json");

            //Post method
            HttpResponseMessage postResponse = client.PostAsync("/Wcc_Rest-0.0.1-SNAPSHOT/postData", stringContent).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (postResponse.IsSuccessStatusCode)
            {
                //post response
                var casefileResponse = postResponse.Content.ReadAsStringAsync().Result;
                //read post response
            }



            // Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
            client.Dispose();
            return "Uspesno";
        }
    }
}